//
//  AppDelegate.h
//  DemoGitLabCI
//
//  Created by Muhammad Mrzok on 2/19/20.
//  Copyright © 2020 Muhammad Mrzok. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>


@end

